#include <Arduino.h>

#ifndef SERVO_H 
#define SERVO_H 
#include <Servo.h>
#endif


#include "sensorInfra.h"


Servo motor;
int servo=2;

void setupServo(){

  pinMode(servo, OUTPUT);
  motor.attach(servo);
  
}


int moverIzquierda(int retardo, int tope ){

  int i;
  float medida;
  int resultado=0;
  int posicion=180;

  for (i = 90; i < 180; i++) {

      motor.write(i);
      enviarSennal();
      medida = medir();
      if(medida<tope){

        posicion=i;
        i=180;
        resultado=1;
        
      }
      delay(retardo);
      

    }

    for (i = posicion; i > 90; i--) {

      motor.write(i);
      delay(retardo);

    }

    return resultado;
}



int moverDerecha(int retardo, int tope){

  int i;
  float medida;
  int resultado=0;
  int posicion=0;
  
  

  
  for(i=90;i>0;i--){

      motor.write(i);
      enviarSennal();
      //La variable medida guarda la medida que da el sensor ultrasonico
      medida=medir();

      if(medida<tope){

          posicion=i;

          i=0;
          resultado=1;
      }

      delay(retardo);
    
  }

  for (i = posicion; i < 90; i++) {

      motor.write(i);
      delay(retardo);

 }

 return resultado;


  
}


/*
 * direccion: Es un entero que indica si gira a la izquierda (1) o a la derecha (0)
 * retardo: El tiempo que quiero que se demore cada paso del giro
 * tope: la distancia en la detecta si hay obstáculo
 * 
 * La función retorna:
 * 0 si todas las medidas son mayores a tope. Es decir, si no hay un obstáculo. 
 * 1 si alguna medición es igual o menor a tope. Es decir si hay un obstáculo a una distancia menor a tope
 */

int moverServo(int direccion, int retardo, int tope) {

  int i;
  int resultado;
  
  if (direccion == 1) {

    resultado=moverIzquierda(retardo, tope);

  } else {


    resultado=moverDerecha(retardo, tope);

  }

  return resultado;

}

void centrar(){
      motor.write(90);
      delay(300);
}
