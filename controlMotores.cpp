
#include <Arduino.h>

int in1 = 16;
int in2 = 5;
int in3 = 4;
int in4 = 0;

void setupControlMotores() {

  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);


}

void girar(int direccion) {
  //Girar a la derecha
  if (direccion == 1) {

    //Parar la rueda Derecha
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);

    //Mover la rueda izquierda hacia adelante
    digitalWrite(in3, LOW);
    digitalWrite(in4, HIGH);
  } else {

    //Parar la rueda Izquierda
    digitalWrite(in3, LOW);
    digitalWrite(in4, LOW);

    //Mover la rueda derecha hacia adelante
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);

  }
}


void mover(int direccion) {
  //Mover hacia adelante
  if (direccion == 1) {

    //Mover rueda derecha hacia adelante
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);

    //Mover rueda izquierda hacia adelante
    digitalWrite(in3, LOW);
    digitalWrite(in4, HIGH);
  } else {

    //Mover rueda derecha hacia atras
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);

    //Mover la rueda izquierda hacia atras
    digitalWrite(in3, HIGH);
    digitalWrite(in4, LOW);

  }
}

void parar() {
  //Parar la rueda Derecha
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);

  //PARA LA RUEDA IZQUIERDA
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);

}
