#include "controlMotores.h"
#include "sensorInfra.h"
#include "controlServo.h"


void setup() {
  setupSensorInfra();
  setupControlMotores();
  setupServo();
  Serial.begin(9600);

}


int retrocedio=false;
void loop() {

  float medida;

  int obstaculo;

  mover(1);
  
  
  enviarSennal();

  medida = medir();
  Serial.println(medida);

  /*
   * El carro para si la medida es <20 o retrocedió
   * 
   */


  if (medida <= 20 || retrocedio==true )
  {
    parar();
    delay(1000);


    /**
     * Generar 1 o 0. Si es 1 el servo girarà a la izquierda buscando si hay obstáculo
     * Si es 0 el servo girará a la derecha buscando un obstáculo
     */
    int aleatorio = random(0,2);
    /*
      * Mover el servo en la dirección que indique la variable aleatorio a una velocidad de 10 ms cada grado
      * y devolverá 0 si no detecta una medida menor a 20 cm.
     */
    obstaculo = moverServo(aleatorio, 10, 20);

    //Si no hay obstáculo
    if (obstaculo == 0) {
      //Poner que el carro no retrocedió
      retrocedio=false;
      //Girar el carro en la misma dirección que giró el servo
      girar(aleatorio);

    int aleatorio=random(0,2);

    obstaculo = moverServo(aleatorio, 10, 20);

    if (obstaculo == 1) {

      obstaculo = moverServo(!aleatorio, 10, 20);
      if(obstaculo==1){
        mover(0);
        retrocedio=true;
        delay(1000);
        }else{
            retrocedio=false;
            girar(aleatorio);
            delay(800);
          
        }
        
    } else {
      retrocedio=false;
      girar(!aleatorio);
      delay(800);
      
        
    }else {
      //Si hay obstáculo
      //Mover el servo en la otra dirección
      obstaculo = moverServo(!aleatorio, 10, 20);

      //No encontró obstáculo en la otra dirección      
      if(obstaculo==0){
        retrocedio=false;
        //Girar el carro en la otra dirección
        girar(!aleatorio);
        delay(800);        
        }else{
            //Si encontró un obstáculo también en esta dirección entonces retroceder el carro
            mover(0);
            retrocedio=true;
            delay(1000);
        }
    }
  }

}
