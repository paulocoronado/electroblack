#ifndef CONTROLMOTORES_H
#define CONTROLMOTORES_H

#include <Arduino.h>

void setupControlMotores();
void girar(int direccion);
void mover(int direccion);
void parar();



#endif
