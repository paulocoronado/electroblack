#include <Arduino.h>

int trigger = 14;
int echo = 12;

void setupSensorInfra() {

  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);

}

void enviarSennal()
{

  digitalWrite(trigger, LOW);
  delayMicroseconds(4);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);

}

float medir() {
  long tiempo;
  float distancia;

  tiempo = pulseIn(echo, HIGH) / 2;

  //d=v*t

  distancia = 0.034 * tiempo;

  return distancia;
}
