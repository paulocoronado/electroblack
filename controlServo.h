#ifndef CONTROLSERVO_H
#define CONTROLSERVO_H

#include <Arduino.h>
#include <Servo.h>

void setupServo();
int moverServo(int direccion, int retardo, int tope);

#endif
